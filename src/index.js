const path = require('path')
const { app, BrowserWindow } = require('electron')
const { is } = require('electron-util')
const Store = require('electron-store')
// const menu = require('./menu')

// Adds debug features like hotkeys for triggering dev tools and reload
if (process.env.NODE_ENV === 'development') {
  require('electron-debug')()
}

// Prevent window being garbage collected
let mainWindow

const defaultConfig = {
  lastWindowState: {
    width: 1024,
    height: 768,
    x: null,
    y: null
  }
}
const config = new Store({ defaults: defaultConfig })

function onClosed() {
  // Dereference the window
  // For multiple windows store them in an array
  mainWindow = null
}


function createMainWindow() {
  const lastWindowState = config.get('lastWindowState')

  const win = new BrowserWindow({
    x: lastWindowState.x,
    y: lastWindowState.y,
    width: lastWindowState.width,
    height: lastWindowState.height,
    minWidth: 1024,
    minHeight: 768,
    autoHideMenuBar: true,
    icon: is.linux ? path.join(__dirname, 'static', 'Icon.png') : undefined,
    webPreferences: {
      preload: path.join(__dirname, 'browser.js')
    }
  })

  win.loadURL('http://deezer.com')

  win.on('close', () => {
    config.set('lastWindowState', win.getNormalBounds())
  })

  win.on('closed', onClosed)

  return win
}

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (!mainWindow) {
    mainWindow = createMainWindow()
  }
})

app.on('ready', () => {
  mainWindow = createMainWindow()
  // Menu.setApplicationMenu(menu)
})
