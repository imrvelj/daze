const { remote } = require('electron')

window.addEventListener('beforeunload', () => {
  if(window.dzPlayer.isPlaying()) {
    document.querySelector('[aria-label="Pause"]').click()
    remote.getCurrentWindow().destroy()
  }
})
