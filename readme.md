# Daze

> Unofficial Deezer desktop app

## Dev

```
$ npm install
```

### Run

```
$ npm start
```

### Build

```
$ npm run build
```
